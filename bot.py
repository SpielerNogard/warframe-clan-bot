import discord
from discord.ext import commands,tasks
import requests
import json
import time

import FunktionenBot
import Hilfe
import embed_generator
import DroptableReader

Token = ''


bot = commands.Bot(command_prefix="!")
def give_time():
    named_tuple = time.localtime() # get struct_time
    time_string = time.strftime("%m/%d/%Y, %H:%M:%S", named_tuple)
    time_string = time_string + " : "
    return(time_string)

def write_log(nachricht):
    
    fobj_out = open("log.txt","a")
    time = give_time()
    fobj_out.write("\r\n"+time + nachricht)
    fobj_out.close()

@bot.event
async def on_ready():
    print(f'{bot.user.name} has connected to Discord!')
    print("Changing Status .......")
    await bot.change_presence(status=discord.Status.idle, activity=discord.Game('Starting ..... '))
    change_status.start()

@tasks.loop(seconds=25)
async def change_status():
    try:
        Out = FunktionenBot.getWorldstateCetusPC()
        await bot.change_presence(status=discord.Status.idle, activity=discord.Game(Out))
        time = give_time()
        print(time +"Status changed")
        write_log("Status changed to: ' "+Out+" '")
        

    except:
        print('Fehler in change_status bot.py')


@bot.event
async def on_member_join(member):
    role = discord.utils.get(member.guild.roles, name ="Freie Tenno")
    await member.add_roles(role)
    await member.send(f'Hi {member.name},')
    await member.send("``` Ich bin der Bot des Discord Relais. Mein Name ist Cephalon Lotus. Bitte lies dir die Regeln durch und halte dich daran. ```")
    write_log("New Member joined : ' "+member.name+" '")

@bot.event
async def on_message(Message):
    try:
        time = give_time()
        Befehl = Message.content
        Befehl = Befehl.lower()
        User = Message.author
        channel = Message.channel
        guild = Message.guild
        print(time +str(User)+" : "+str(Message.content))
        write_log("new Message : ' "+str(User)+": " +str(Message.content)+" '")
        #Hilfe Fenster
        if Befehl.startswith("!help"):
            embed = Hilfe.HilfePC()
            await User.send(embed=embed)
        if Befehl.startswith("&help"):
            embed = Hilfe.HilfePS4()
            await User.send(embed=embed)
        if Befehl.startswith("?help"):
            embed = Hilfe.HilfeXB1()
            await User.send(embed=embed)
        if Befehl.startswith("%help"):
            embed = Hilfe.HilfeSWI()
            await User.send(embed=embed)
        #Role Assignment
        if Befehl.startswith("!pc"):
            role = discord.utils.get(guild.roles, name = "Tenno PC")
            await User.add_roles(role)
        if Befehl.startswith("&ps4"):
            role = discord.utils.get(guild.roles, name = "Tenno Playstation 4")
            await User.add_roles(role)
        if Befehl.startswith("?xb1"):
            role = discord.utils.get(guild.roles, name = "Tenno Xbox One")
            await User.add_roles(role)
        if Befehl.startswith("%swi"):
            role = discord.utils.get(guild.roles, name = "Tenno Nintendo Switch")
            await User.add_roles(role)
        #Arbitration
        if Befehl.startswith("!arbitration"):
            embed = embed_generator.generate_embed_Arbitration("PC")
            await User.send(embed=embed)
        if Befehl.startswith("%arbitration"):
            embed = embed_generator.generate_embed_Arbitration("SWI")
            await User.send(embed=embed)
        if Befehl.startswith("&arbitration"):
            embed = embed_generator.generate_embed_Arbitration("PS4")
            await User.send(embed=embed)
        if Befehl.startswith("?arbitration"):
            embed = embed_generator.generate_embed_Arbitration("XB1")
            await User.send(embed=embed)
        #Riven Preise
        if Befehl.startswith("!rivenprice"):
           Suche = Befehl
           Search = Suche.replace("!rivenprice ", "")
           embed= FunktionenBot.getRivensPC(Search)
           await User.send(embed=embed)
        if Befehl.startswith("&rivenprice"):
           Suche = Befehl
           Search = Suche.replace("&rivenprice ", "")
           embed= FunktionenBot.getRivensPS4(Search)
           await User.send(embed=embed)
        if Befehl.startswith("?rivenprice"):
           Suche = Befehl
           Search = Suche.replace("?rivenprice ", "")
           embed= FunktionenBot.getRivensXB1(Search)
           await User.send(embed=embed)
        if Befehl.startswith("%rivenprice"):
           Suche = Befehl
           Search = Suche.replace("%rivenprice ", "")
           embed= FunktionenBot.getRivensSWI(Search)
           await User.send(embed=embed)
        #Weapon Stats
        if Befehl.startswith("!weapon"):
           Suche = Befehl
           Weapon = Suche.replace("!weapon ", "")
           embed= FunktionenBot.Weapon(Weapon)
           await User.send(embed=embed)
        if Befehl.startswith("?weapon"):
           Suche = Befehl
           Weapon = Suche.replace("?weapon ", "")
           embed= FunktionenBot.Weapon(Weapon)
           await User.send(embed=embed)
        if Befehl.startswith("%weapon"):
           Suche = Befehl
           Weapon = Suche.replace("%weapon ", "")
           embed= FunktionenBot.Weapon(Weapon)
           await User.send(embed=embed)
        if Befehl.startswith("&weapon"):
           Suche = Befehl
           Weapon = Suche.replace("&weapon ", "")
           embed= FunktionenBot.Weapon(Weapon)
           await User.send(embed=embed)
        #Outpost
        if Befehl.startswith("!outpost"):
            embed = embed_generator.generate_embed_Outpost("PC")
            await User.send(embed=embed)
        if Befehl.startswith("%outpost"):
            embed = embed_generator.generate_embed_Outpost("SWI")
            await User.send(embed=embed)
        if Befehl.startswith("&outpost"):
            embed = embed_generator.generate_embed_Outpost("PS4")
            await User.send(embed=embed)
        if Befehl.startswith("?outpost"):
            embed = embed_generator.generate_embed_Outpost("XB1")
            await User.send(embed=embed)
        #Darvo
        if Befehl.startswith("!darvo"):
            embed = embed_generator.generate_embed_darvo("PC")
            await User.send(embed=embed)
        if Befehl.startswith("%darvo"):
            embed = embed_generator.generate_embed_darvo("SWI")
            await User.send(embed=embed)
        if Befehl.startswith("&darvo"):
            embed = embed_generator.generate_embed_darvo("PS4")
            await User.send(embed=embed)
        if Befehl.startswith("?darvo"):
            embed = embed_generator.generate_embed_darvo("XB1")
            await User.send(embed=embed)
        #fissures
        if Befehl.startswith("!fissures"):
            message = FunktionenBot.get_fissures_PC()
            await User.send("```"+message+"```")
        if Befehl.startswith("%fissures"):
            message = FunktionenBot.get_fissures_SWI()
            await User.send("```"+message+"```")
        if Befehl.startswith("&fissures"):
            message = FunktionenBot.get_fissures_PS4()
            await User.send("```"+message+"```")
        if Befehl.startswith("?fissures"):
            message = FunktionenBot.get_fissures_XB1()
            await User.send("```"+message+"```")
        #Droptable Reader
        if Befehl.startswith("!search"):
           Suche = Befehl
           Weapon = Suche.replace("!search ", "")
           message = DroptableReader.vollständige_suche(Weapon)
           await User.send("```"+message+"```")
        if Befehl.startswith("%search"):
           Suche = Befehl
           Weapon = Suche.replace("!search ", "")
           message = DroptableReader.vollständige_suche(Weapon)
           await User.send("```"+message+"```")
        if Befehl.startswith("?search"):
           Suche = Befehl
           Weapon = Suche.replace("!search ", "")
           message = DroptableReader.vollständige_suche(Weapon)
           await User.send("```"+message+"```")
        if Befehl.startswith("&search"):
           Suche = Befehl
           Weapon = Suche.replace("!search ", "")
           message = DroptableReader.vollständige_suche(Weapon)
           await User.send("```"+message+"```")
        #Warframe market seller
        if Befehl.startswith("!seller"):
            Suche = Message.content
            Eingabe = Suche.replace("!seller ", "")
            Item = Eingabe
            message = FunktionenBot.warframe_market(Item)
            await User.send("```"+message+"```")
        write_log("Befehl erfolgreich ausgeführt : ' "+str(User)+": " +str(Message.content)+" '")
        

    except:
        
        Fehler = "Es ist ein Fehler aufgetreten:"
        write_log("Es ist ein Fehler aufgetreten : ' "+str(User)+": " +str(Message.content)+"'")
        print(Fehler)

bot.run(Token)







        

import requests
import json
import discord



def getWorldstateCetusPC():
    try:
        url='https://api.warframestat.us/pc/cetusCycle/'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Out = python_json_obj["shortString"]
            print(Out)
    except:
        Out = "Error while searching for WorldStatus Cetus"
    return Out

def getArbitartionDataPC():
    try:
        url='https://api.warframestat.us/pc/arbitration'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Out = python_json_obj["node"]
            Out1 = python_json_obj["enemy"]
            Out2 = python_json_obj["type"]
            Out3 = python_json_obj["activation"] 
            Out4 = python_json_obj["expiry"]
            Arbitration = 'Ort: '+Out+' ('+Out1+') '+Out2
    except:
        Arbitration = "Error while checking Arbitration Status"
    return Arbitration,Out,Out1,Out2,Out3,Out4  
def getArbitartionDataSWI():
    try:
        url='https://api.warframestat.us/swi/arbitration'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Out = python_json_obj["node"]
            Out1 = python_json_obj["enemy"]
            Out2 = python_json_obj["type"]
            Out3 = python_json_obj["activation"] 
            Out4 = python_json_obj["expiry"]
            Arbitration = 'Ort: '+Out+' ('+Out1+') '+Out2
    except:
        Arbitration = "Error while checking Arbitration Status"
    return Arbitration,Out,Out1,Out2,Out3,Out4  

def getArbitartionDataPS4():
    try:
        url='https://api.warframestat.us/ps4/arbitration'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Out = python_json_obj["node"]
            Out1 = python_json_obj["enemy"]
            Out2 = python_json_obj["type"]
            Out3 = python_json_obj["activation"] 
            Out4 = python_json_obj["expiry"]
            Arbitration = 'Ort: '+Out+' ('+Out1+') '+Out2
    except:
        Arbitration = "Error while checking Arbitration Status"
    return Arbitration,Out,Out1,Out2,Out3,Out4 

def getArbitartionDataXB1():
    try:
        url='https://api.warframestat.us/xb1/arbitration'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Out = python_json_obj["node"]
            Out1 = python_json_obj["enemy"]
            Out2 = python_json_obj["type"]
            Out3 = python_json_obj["activation"] 
            Out4 = python_json_obj["expiry"]
            Arbitration = 'Ort: '+Out+' ('+Out1+') '+Out2
    except:
        Arbitration = "Error while checking Arbitration Status"
    return Arbitration,Out,Out1,Out2,Out3,Out4  
def getRivensPC(Search):
    url='https://api.warframestat.us/PC/rivens/search/'+Search
    response = requests.get(url, stream=True)
    for line in response.iter_lines(decode_unicode=True, delimiter="}"):
        try:
            info=line.split("{")[1]
            info2="{"+info+"}"
            python_json_obj = json.loads(info2)
            Out1 = python_json_obj["itemType"]
            Out2 = python_json_obj["compatibility"]
            Out3 = python_json_obj["rerolled"]
            Out5 = python_json_obj["avg"]
            Out4 = python_json_obj["stddev"]
            Out6 = python_json_obj["min"]
            Out7 = python_json_obj["max"]
            Out8 = python_json_obj["pop"]
            Out9 = python_json_obj["median"]
            Out = "Dein gesuchter Riven: "+"\n"+"Riven Art: "+Out1+"\n"+"Waffe: "+Out2+"\n"+"gerollt?: "+str(Out3)+"\n"+"Lowest Price: "+str(Out6)+"\n"+"Highest Price: "+str(Out7)
            print(Out) 
            embed=discord.Embed(title="Riven Preis", description="Hier ist dein gewünschter Preis", color=0x0080ff)
            embed.set_thumbnail(url="https://imgs3.forfansbyfans.com/img/cache/fan/16/4996.jpg-780x780.jpg")
            embed.add_field(name="Waffe", value=Out2, inline=False)
            embed.add_field(name="Riven Art", value=Out1, inline=False)
            embed.add_field(name="gerollt", value=str(Out3), inline=False)
            embed.add_field(name="Popularität", value=str(Out8), inline=False)
            embed.add_field(name="Kleinster Preis", value=str(Out6), inline=False)
            embed.add_field(name="Höchster Preis", value=str(Out7), inline=False)
            embed.add_field(name="Durchschnitt", value=str(Out9), inline=False)

            return (embed)
        except:
            info="Fehler"
            print(info)
def getRivensSWI(Search):
    url='https://api.warframestat.us/swi/rivens/search/'+Search
    response = requests.get(url, stream=True)
    for line in response.iter_lines(decode_unicode=True, delimiter="}"):
        try:
            info=line.split("{")[1]
            info2="{"+info+"}"
            python_json_obj = json.loads(info2)
            Out1 = python_json_obj["itemType"]
            Out2 = python_json_obj["compatibility"]
            Out3 = python_json_obj["rerolled"]
            Out5 = python_json_obj["avg"]
            Out4 = python_json_obj["stddev"]
            Out6 = python_json_obj["min"]
            Out7 = python_json_obj["max"]
            Out8 = python_json_obj["pop"]
            Out9 = python_json_obj["median"]
            Out = "Dein gesuchter Riven: "+"\n"+"Riven Art: "+Out1+"\n"+"Waffe: "+Out2+"\n"+"gerollt?: "+str(Out3)+"\n"+"Lowest Price: "+str(Out6)+"\n"+"Highest Price: "+str(Out7)
            print(Out) 
            embed=discord.Embed(title="Riven Preis", description="Hier ist dein gewünschter Preis", color=0x0080ff)
            embed.set_thumbnail(url="https://imgs3.forfansbyfans.com/img/cache/fan/16/4996.jpg-780x780.jpg")
            embed.add_field(name="Waffe", value=Out2, inline=False)
            embed.add_field(name="Riven Art", value=Out1, inline=False)
            embed.add_field(name="gerollt", value=str(Out3), inline=False)
            embed.add_field(name="Popularität", value=str(Out8), inline=False)
            embed.add_field(name="Kleinster Preis", value=str(Out6), inline=False)
            embed.add_field(name="Höchster Preis", value=str(Out7), inline=False)
            embed.add_field(name="Durchschnitt", value=str(Out9), inline=False)

            return (embed)
        except:
            info="Fehler"
            print(info) 
def getRivensPS4(Search):
    url='https://api.warframestat.us/ps4/rivens/search/'+Search
    response = requests.get(url, stream=True)
    for line in response.iter_lines(decode_unicode=True, delimiter="}"):
        try:
            info=line.split("{")[1]
            info2="{"+info+"}"
            python_json_obj = json.loads(info2)
            Out1 = python_json_obj["itemType"]
            Out2 = python_json_obj["compatibility"]
            Out3 = python_json_obj["rerolled"]
            Out5 = python_json_obj["avg"]
            Out4 = python_json_obj["stddev"]
            Out6 = python_json_obj["min"]
            Out7 = python_json_obj["max"]
            Out8 = python_json_obj["pop"]
            Out9 = python_json_obj["median"]
            Out = "Dein gesuchter Riven: "+"\n"+"Riven Art: "+Out1+"\n"+"Waffe: "+Out2+"\n"+"gerollt?: "+str(Out3)+"\n"+"Lowest Price: "+str(Out6)+"\n"+"Highest Price: "+str(Out7)
            print(Out) 
            embed=discord.Embed(title="Riven Preis", description="Hier ist dein gewünschter Preis", color=0x0080ff)
            embed.set_thumbnail(url="https://imgs3.forfansbyfans.com/img/cache/fan/16/4996.jpg-780x780.jpg")
            embed.add_field(name="Waffe", value=Out2, inline=False)
            embed.add_field(name="Riven Art", value=Out1, inline=False)
            embed.add_field(name="gerollt", value=str(Out3), inline=False)
            embed.add_field(name="Popularität", value=str(Out8), inline=False)
            embed.add_field(name="Kleinster Preis", value=str(Out6), inline=False)
            embed.add_field(name="Höchster Preis", value=str(Out7), inline=False)
            embed.add_field(name="Durchschnitt", value=str(Out9), inline=False)

            return (embed)
        except:
            info="Fehler"
            print(info) 
def getRivensXB1(Search):
    url='https://api.warframestat.us/xb1/rivens/search/'+Search
    response = requests.get(url, stream=True)
    for line in response.iter_lines(decode_unicode=True, delimiter="}"):
        try:
            info=line.split("{")[1]
            info2="{"+info+"}"
            python_json_obj = json.loads(info2)
            Out1 = python_json_obj["itemType"]
            Out2 = python_json_obj["compatibility"]
            Out3 = python_json_obj["rerolled"]
            Out5 = python_json_obj["avg"]
            Out4 = python_json_obj["stddev"]
            Out6 = python_json_obj["min"]
            Out7 = python_json_obj["max"]
            Out8 = python_json_obj["pop"]
            Out9 = python_json_obj["median"]
            Out = "Dein gesuchter Riven: "+"\n"+"Riven Art: "+Out1+"\n"+"Waffe: "+Out2+"\n"+"gerollt?: "+str(Out3)+"\n"+"Lowest Price: "+str(Out6)+"\n"+"Highest Price: "+str(Out7)
            print(Out) 
            embed=discord.Embed(title="Riven Preis", description="Hier ist dein gewünschter Preis", color=0x0080ff)
            embed.set_thumbnail(url="https://imgs3.forfansbyfans.com/img/cache/fan/16/4996.jpg-780x780.jpg")
            embed.add_field(name="Waffe", value=Out2, inline=False)
            embed.add_field(name="Riven Art", value=Out1, inline=False)
            embed.add_field(name="gerollt", value=str(Out3), inline=False)
            embed.add_field(name="Popularität", value=str(Out8), inline=False)
            embed.add_field(name="Kleinster Preis", value=str(Out6), inline=False)
            embed.add_field(name="Höchster Preis", value=str(Out7), inline=False)
            embed.add_field(name="Durchschnitt", value=str(Out9), inline=False)

            return (embed)
        except:
            info="Fehler"
            print(info)

def WeaponCategory(Weapon):
    try:
        url='https://api.warframestat.us/weapons/'+Weapon
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            Category = python_json_obj["category"]
    except:
        print("Konnte Kategorie nicht feststellen")  
        Category = "Konnte Kategorie nicht feststellen"
    return(Category)

def Weapon(Weapon):
    try:
        Category = WeaponCategory(Weapon)
        if Category == "Secondary":
            url='https://api.warframestat.us/weapons/'+Weapon
            with requests.get(url) as response:
                #Json Object erhalten
                python_json_obj = response.text
                python_json_obj = json.loads(python_json_obj)
                #Json Object auslesen
                Name = python_json_obj["name"]
                Beschreibung = python_json_obj["description"]
                Bild = python_json_obj["wikiaThumbnail"]
                MagazinGröße = python_json_obj["magazineSize"]
                Nachladezeit = python_json_obj["reloadTime"]
                CritChance = python_json_obj["criticalChance"]
                CritMultiplier = python_json_obj["criticalMultiplier"]
                Damage = python_json_obj["damage"]
                Damageverteilung = python_json_obj["damageTypes"]
                Firerate = python_json_obj["fireRate"]
                Statuschance = python_json_obj["procChance"]
                Mastery = python_json_obj["masteryReq"]
                Riven = python_json_obj["disposition"]
                Noise = python_json_obj["noise"]
                Link = python_json_obj["wikiaUrl"]
                #Ausgabe
                embed=discord.Embed(title=str(Name), description="Deine Ergebnisse: ", color=0xff0000)
                embed.set_thumbnail(url=Bild)
                embed.add_field(name="Beschreibung", value=str(Beschreibung), inline=False)
                embed.add_field(name="MagazinGröße", value=str(MagazinGröße), inline=True)
                embed.add_field(name="Nachladezeit", value=str(Nachladezeit), inline=True)
                embed.add_field(name="Crit Chance", value=str(CritChance), inline=True)
                embed.add_field(name="Crit Multiplier", value=str(CritMultiplier), inline=True)
                embed.add_field(name="Statuschance", value=str(Statuschance), inline=True)
                embed.add_field(name="Damage", value=str(Damage), inline=True)
                embed.add_field(name="Damage Verteilung", value=str(Damageverteilung), inline=True)
                embed.add_field(name="Firerate", value=str(Firerate), inline=True)
                embed.add_field(name="Benötigter Mastery Rank", value=str(Mastery), inline=True)
                embed.add_field(name="Riven Dispo", value=str(Riven), inline=True)
                embed.add_field(name="Lautstärke", value=str(Noise), inline=True)
                embed.add_field(name="Category", value="Secondary", inline=True)
                embed.add_field(name="mehr Informationen", value=str(Link), inline=True)
                
        if Category == "Melee":
            url='https://api.warframestat.us/weapons/'+Weapon
            with requests.get(url) as response:
                #Json Object erhalten
                python_json_obj = response.text
                python_json_obj = json.loads(python_json_obj)
                #Json Object auslesen
                Name = python_json_obj["name"]
                Beschreibung = python_json_obj["description"]
                Bild = python_json_obj["wikiaThumbnail"]
                Attackspeed = python_json_obj["fireRate"]
                CritChance = python_json_obj["criticalChance"]
                CritMultiplier = python_json_obj["criticalMultiplier"]
                Damage = python_json_obj["damage"]
                Damageverteilung = python_json_obj["damageTypes"]
                Firerate = python_json_obj["fireRate"]
                Statuschance = python_json_obj["procChance"]
                Mastery = python_json_obj["masteryReq"]
                Riven = python_json_obj["disposition"]
                Spin = python_json_obj["slamAttack"]
                Leap = python_json_obj["slideAttack"]
                Wall = python_json_obj["heavySlamAttack"]
                Link = python_json_obj["wikiaUrl"]
                Typ = python_json_obj["type"]
                #Ausgabe
                embed=discord.Embed(title=str(Name), description="Deine Ergebnisse: ", color=0xff0000)
                embed.set_thumbnail(url=Bild)
                embed.add_field(name="Beschreibung", value=str(Beschreibung), inline=False)
                embed.add_field(name="Typ", value=str(Typ), inline=True)
                embed.add_field(name="Attackspeed", value=str(Attackspeed), inline=True)
                embed.add_field(name="Crit Chance", value=str(CritChance), inline=True)
                embed.add_field(name="Crit Multiplier", value=str(CritMultiplier), inline=True)
                embed.add_field(name="Statuschance", value=str(Statuschance), inline=True)
                embed.add_field(name="Damage", value=str(Damage), inline=True)
                embed.add_field(name="Damage Verteilung", value=str(Damageverteilung), inline=True)
                embed.add_field(name="Benötigter Mastery Rank", value=str(Mastery), inline=True)
                embed.add_field(name="Riven Dispo", value=str(Riven), inline=True)
                embed.add_field(name="Slam Attack", value=str(Spin), inline=True)
                embed.add_field(name="Slide Attack", value=str(Leap), inline=True)
                embed.add_field(name="Heavy Slam Attack", value=str(Wall), inline=True)
                embed.add_field(name="Category", value="Melee", inline=True)
                embed.add_field(name="mehr Informationen", value=str(Link), inline=True)
                
        if Category == "Primary":
            url='https://api.warframestat.us/weapons/'+Weapon
            with requests.get(url) as response:
                #Json Object erhalten
                python_json_obj = response.text
                python_json_obj = json.loads(python_json_obj)
                #Json Object auslesen
                Name = python_json_obj["name"]
                Beschreibung = python_json_obj["description"]
                Bild = python_json_obj["wikiaThumbnail"]
                MagazinGröße = python_json_obj["magazineSize"]
                Nachladezeit = python_json_obj["reloadTime"]
                CritChance = python_json_obj["criticalChance"]
                CritMultiplier = python_json_obj["criticalMultiplier"]
                Damage = python_json_obj["damage"]
                Damageverteilung = python_json_obj["damageTypes"]
                Firerate = python_json_obj["fireRate"]
                Statuschance = python_json_obj["procChance"]
                Mastery = python_json_obj["masteryReq"]
                Riven = python_json_obj["disposition"]
                Noise = python_json_obj["noise"]
                Link = python_json_obj["wikiaUrl"]
                #Ausgabe
                embed=discord.Embed(title=str(Name), description="Deine Ergebnisse: ", color=0xff0000)
                embed.set_thumbnail(url=Bild)
                embed.add_field(name="Beschreibung", value=str(Beschreibung), inline=False)
                embed.add_field(name="MagazinGröße", value=str(MagazinGröße), inline=True)
                embed.add_field(name="Nachladezeit", value=str(Nachladezeit), inline=True)
                embed.add_field(name="Crit Chance", value=str(CritChance), inline=True)
                embed.add_field(name="Crit Multiplier", value=str(CritMultiplier), inline=True)
                embed.add_field(name="Statuschance", value=str(Statuschance), inline=True)
                embed.add_field(name="Damage", value=str(Damage), inline=True)
                embed.add_field(name="Damage Verteilung", value=str(Damageverteilung), inline=True)
                embed.add_field(name="Firerate", value=str(Firerate), inline=True)
                embed.add_field(name="Benötigter Mastery Rank", value=str(Mastery), inline=True)
                embed.add_field(name="Riven Dispo", value=str(Riven), inline=True)
                embed.add_field(name="Lautstärke", value=str(Noise), inline=True)
                embed.add_field(name="Category", value="Primary", inline=True)
                embed.add_field(name="mehr Informationen", value=str(Link), inline=True)
    except:
        print("Fehler in Weapon")  
        
        embed=discord.Embed(title="Fehler", description="Deine Ergebnisse: ", color=0xff0000)
        embed.set_thumbnail('https://imnamenderliebe.com/wp-content/uploads/2015/01/Fehler.jpg')
        embed.add_field(name="Beschreibung", value="Es ist Leider ein Fehler aufgetreten", inline=False)
    return(embed)

def get_Sentient_OutpostPC():
    try:
        url1='https://api.warframestat.us/PC/sentientOutposts'
        with requests.get(url1) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            mission1 = python_json_obj["mission"]
            active1 = python_json_obj["active"]
            mission_1 = json.dumps(mission1)
            python_json_obj1 = json.loads(mission_1)
            node1 = python_json_obj1["node"]
            faction1 = python_json_obj1["faction"]
            art1 = python_json_obj1["type"]
            node1 = node1.replace("'", " ")
        return(node1,faction1,art1,active1,mission1)
    except:
        print("Fehler in Sentient Outpost PC")
def get_Sentient_OutpostSWI():
    try:
        url1='https://api.warframestat.us/swi/sentientOutposts'
        with requests.get(url1) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            mission1 = python_json_obj["mission"]
            active1 = python_json_obj["active"]
            mission_1 = json.dumps(mission1)
            python_json_obj1 = json.loads(mission_1)
            node1 = python_json_obj1["node"]
            faction1 = python_json_obj1["faction"]
            art1 = python_json_obj1["type"]
            node1 = node1.replace("'", " ")
        return(node1,faction1,art1,active1,mission1)
    except:
        print("Fehler in Sentient Outpost Swi")

def get_Sentient_OutpostPS4():
    try:
        url1='https://api.warframestat.us/ps4/sentientOutposts'
        with requests.get(url1) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            mission1 = python_json_obj["mission"]
            active1 = python_json_obj["active"]
            mission_1 = json.dumps(mission1)
            python_json_obj1 = json.loads(mission_1)
            node1 = python_json_obj1["node"]
            faction1 = python_json_obj1["faction"]
            art1 = python_json_obj1["type"]
            node1 = node1.replace("'", " ")
        return(node1,faction1,art1,active1,mission1)
    except:
        print("Fehler in Sentient Outpost PS4")

def get_Sentient_OutpostPC():
    try:
        url1='https://api.warframestat.us/xb1/sentientOutposts'
        with requests.get(url1) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            mission1 = python_json_obj["mission"]
            active1 = python_json_obj["active"]
            mission_1 = json.dumps(mission1)
            python_json_obj1 = json.loads(mission_1)
            node1 = python_json_obj1["node"]
            faction1 = python_json_obj1["faction"]
            art1 = python_json_obj1["type"]
            node1 = node1.replace("'", " ")
        return(node1,faction1,art1,active1,mission1)
    except:
        print("Fehler in Sentient Outpost XB1")

def Darvo_Deal_PC():
        url='https://api.warframestat.us/PC/dailyDeals'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["item"]
                Out2 = python_json_obj["originalPrice"]
                Out3 = python_json_obj["salePrice"]
                Out4 = python_json_obj["total"]
                Out5 = python_json_obj["sold"]
                Out6 = python_json_obj["eta"]
                
            except:
                print("Fehler in PC Darvo")
        return(Out1,Out2,Out3,Out4,Out5,Out6)

def Darvo_Deal_SWI():
        url='https://api.warframestat.us/swi/dailyDeals'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["item"]
                Out2 = python_json_obj["originalPrice"]
                Out3 = python_json_obj["salePrice"]
                Out4 = python_json_obj["total"]
                Out5 = python_json_obj["sold"]
                Out6 = python_json_obj["eta"]
                
            except:
                print("Fehler in PC Darvo")
        return(Out1,Out2,Out3,Out4,Out5,Out6)

def Darvo_Deal_PS4():
        url='https://api.warframestat.us/ps4/dailyDeals'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["item"]
                Out2 = python_json_obj["originalPrice"]
                Out3 = python_json_obj["salePrice"]
                Out4 = python_json_obj["total"]
                Out5 = python_json_obj["sold"]
                Out6 = python_json_obj["eta"]
                
            except:
                print("Fehler in PC Darvo")
        return(Out1,Out2,Out3,Out4,Out5,Out6)

def Darvo_Deal_XB1():
        url='https://api.warframestat.us/xb1/dailyDeals'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["item"]
                Out2 = python_json_obj["originalPrice"]
                Out3 = python_json_obj["salePrice"]
                Out4 = python_json_obj["total"]
                Out5 = python_json_obj["sold"]
                Out6 = python_json_obj["eta"]
                
            except:
                print("Fehler in PC Darvo")
        return(Out1,Out2,Out3,Out4,Out5,Out6)

def get_fissures_PC():
        Ausgabe = "Hier sind die aktuellen Void Fissures \n"
        #PC
        url='https://api.warframestat.us/PC/fissures'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["node"]
                Out2 = python_json_obj["missionType"]
                Out3 = python_json_obj["enemy"]
                Out5 = python_json_obj["tier"]
                Out4 = python_json_obj["eta"]
                Out = Out5+": "+Out2+" ("+Out3+") "+Out1+" Time left: "+Out4 + "\n"
                print(Out) 
                Ausgabe = Ausgabe + Out 
                
        


            except:
                info="Fehler in fissures PC"
                print(info)
        
        
        return(Ausgabe)

def get_fissures_SWI():
        Ausgabe = "Hier sind die aktuellen Void Fissures \n"
        #PC
        url='https://api.warframestat.us/swi/fissures'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["node"]
                Out2 = python_json_obj["missionType"]
                Out3 = python_json_obj["enemy"]
                Out5 = python_json_obj["tier"]
                Out4 = python_json_obj["eta"]
                Out = Out5+": "+Out2+" ("+Out3+") "+Out1+" Time left: "+Out4 + "\n"
                print(Out) 
                Ausgabe = Ausgabe + Out 
                
        


            except:
                info="Fehler in fissures PC"
                print(info)
        
        
        return(Ausgabe)
def get_fissures_XB1():
        Ausgabe = "Hier sind die aktuellen Void Fissures \n"
        #PC
        url='https://api.warframestat.us/xb1/fissures'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["node"]
                Out2 = python_json_obj["missionType"]
                Out3 = python_json_obj["enemy"]
                Out5 = python_json_obj["tier"]
                Out4 = python_json_obj["eta"]
                Out = Out5+": "+Out2+" ("+Out3+") "+Out1+" Time left: "+Out4 + "\n"
                print(Out) 
                Ausgabe = Ausgabe + Out 
                
        


            except:
                info="Fehler in fissures PC"
                print(info)
        
        
        return(Ausgabe)
def get_fissures_PS4():
        Ausgabe = "Hier sind die aktuellen Void Fissures \n"
        #PC
        url='https://api.warframestat.us/ps4/fissures'
        response = requests.get(url, stream=True)
        for line in response.iter_lines(decode_unicode=True, delimiter="}"):
            try:
                info=line.split("{")[1]
                info2="{"+info+"}"
                python_json_obj = json.loads(info2)
                Out1 = python_json_obj["node"]
                Out2 = python_json_obj["missionType"]
                Out3 = python_json_obj["enemy"]
                Out5 = python_json_obj["tier"]
                Out4 = python_json_obj["eta"]
                Out = Out5+": "+Out2+" ("+Out3+") "+Out1+" Time left: "+Out4 + "\n"
                print(Out) 
                Ausgabe = Ausgabe + Out 
                
        


            except:
                info="Fehler in fissures PC"
                print(info)
        
        
        return(Ausgabe)
def MarketEingabe(Eingabe):
    Eingabe = Eingabe.replace(" ", "_")
    Eingabe = Eingabe.lower()
    return(Eingabe)

def warframe_market(Eingabe):
    try:
        j=0
        Item = Eingabe
        Ausgabe = "Hier sind die 5 billigsten Verkäufer von "+str(Eingabe)+" auf Warframe Market welche gerade online sind\n"
        print(Eingabe)
        Eingabe = MarketEingabe(Eingabe) 
        
        url='https://api.warframe.market/v1/items/'+Eingabe+'/orders'
        with requests.get(url) as response:
            python_json_obj = response.text
            python_json_obj = json.loads(python_json_obj)
            mission1 = python_json_obj["payload"]
            mission1 = json.dumps(mission1)
            python_json_obj = json.loads(mission1)
            mission2 = python_json_obj["orders"]
            for x in mission2:
                #print(str(x))
                Angebot = json.dumps(x)
                python_json_obj = json.loads(Angebot)
                quantity = python_json_obj["quantity"]
                platinum = python_json_obj["platinum"]
                order_type = python_json_obj["order_type"]
                user = python_json_obj["user"]
                platform = python_json_obj["platform"]
                region = python_json_obj["region"]
                creation_date = python_json_obj["creation_date"]
                last_update = python_json_obj["last_update"]
                visible = python_json_obj["visible"]
                id = python_json_obj["id"]
                if(order_type == "sell"):
                    Verkäufer = json.dumps(user)
                    python_json_obj2 = json.loads(Verkäufer)
                    reputation = python_json_obj2["reputation"]
                    reputation_bonus = python_json_obj2["reputation_bonus"]
                    Verkäufer_Region = python_json_obj2["region"]
                    last_seen = python_json_obj2["last_seen"]
                    ingame_name = python_json_obj2["ingame_name"]
                    status = python_json_obj2["status"]
                    Verkäufer_id = python_json_obj2["id"]
                    avatar = python_json_obj2["avatar"]
                    if(status != "offline"):
                        if j < 7:
                            quantity = int(quantity)
                            platinum = int(platinum)
                            Ausgabe2 = "Username: "+str(ingame_name)+" Status: "+str(status)+" Anzahl: "+str(quantity)+" Platinum: "+str(platinum)+" Angebotstyp: "+str(order_type)+" Plattform: "+str(platform)+" Region: "+str(region)
                            Ausgabe = Ausgabe +Ausgabe2+"\n"
                            Ausgabe = Ausgabe + "Wenn du dieses Item kaufen willst, bitte folgendes ingame eingeben \n"
                            Befehl = "/w "+str(ingame_name)+" Hi I want to buy "+str(Item)+" for "+str(platinum)+" platinum (Warframe Market)"
                            Ausgabe = Ausgabe+Befehl
                            Ausgabe = Ausgabe + "\n"
                            j = j+1
                            print(Ausgabe)
                        print(str(ingame_name))
                        print(" ")
                        print(str(status))
                        print("Anzahl: "+str(quantity))
                        print("Platinum: "+str(platinum))
                        print("Angebotstyp: "+str(order_type))
                        #print("Benutzer: "+str(user))
                        print("Plattform: "+str(platform))
                        print("Region: "+str(region))
                        print("ErstellungsDatum: "+str(creation_date))
                        print("Zuletzt geupdated: "+str(last_update))
                        print("Sichtbar: "+str(visible))
                        print("id: "+str(id))
                        print("------------------------------------------------------------------------------------------------------------------------------------------")
                        print("------------------------------------------------------------------------------------------------------------------------------------------")
            return(Ausgabe)
    except:
        print("Fehler in Warframe MArket")
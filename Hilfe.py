
import discord
from discord.ext import commands, tasks



def HilfePC():
    embed=discord.Embed(title="Help", description="my help page", color=0x0080ff)
    embed.set_thumbnail(url="")
    embed.add_field(name="&Arbitration", value="Zeigt die aktuelle Arbitration an", inline=False)
    embed.add_field(name="&Rivenprice {arg1}", value="Zeigt dir den Preis eines Rivens z.B &Rivenprice War", inline=False)
    embed.add_field(name="&Weapon {arg1}", value="Zeigt dir verschiedenste Weaponstats z.B &Weapon Rubico", inline=False)
    embed.add_field(name="&Outpost", value="Zeigt dir ob eine Sentient Anomaly vorhanden ist ", inline=False)
    embed.add_field(name="&Darvo", value="Zeigt den aktuellen Deal für deine Plattform an ", inline=False)
    embed.add_field(name="&fissures", value="Zeigt die aktuellen Voidfissures anZeigt dir ob eine Sentient Anomaly vorhanden ist ", inline=False)
    embed.add_field(name="&buy {arg1}", value="Zeigt dir für dein Item die 5 meistbietenden Käufer auf Warframe Market an", inline=False)
    embed.add_field(name="&seller {arg1}", value="Zeigt dir für dein Item die 5 billigsten Verkäufer auf Warframe", inline=False)
    embed.add_field(name="&Arcane {arg1}", value="Zeigt verschiedneste Daten zu den Arkanas an ", inline=False)
    return(embed)
def HilfeSWI():
    embed=discord.Embed(title="Help", description="my help page", color=0x0080ff)
    embed.set_thumbnail(url="")
    embed.add_field(name="%Arbitration", value="Zeigt die aktuelle Arbitration an", inline=False)
    embed.add_field(name="%Rivenprice {arg1}", value="Zeigt dir den Preis eines Rivens z.B %Rivenprice War", inline=False)
    embed.add_field(name="%Weapon {arg1}", value="Zeigt dir verschiedenste Weaponstats z.B %Weapon Rubico", inline=False)
    embed.add_field(name="%Outpost", value="Zeigt dir ob eine Sentient Anomaly vorhanden ist ", inline=False)
    embed.add_field(name="%Darvo", value="Zeigt den aktuellen Deal für deine Plattform an ", inline=False)
    embed.add_field(name="%fissures", value="Zeigt die aktuellen Voidfissures anZeigt dir ob eine Sentient Anomaly vorhanden ist ", inline=False)
    embed.add_field(name="%buy {arg1}", value="Zeigt dir für dein Item die 5 meistbietenden Käufer auf Warframe Market an", inline=False)
    embed.add_field(name="%seller {arg1}", value="Zeigt dir für dein Item die 5 billigsten Verkäufer auf Warframe", inline=False)
    embed.add_field(name="%Arcane {arg1}", value="Zeigt verschiedneste Daten zu den Arkanas an ", inline=False)
    return(embed)
def HilfeXB1():
    embed=discord.Embed(title="Help", description="my help page", color=0x0080ff)
    embed.set_thumbnail(url="")
    embed.add_field(name="?Arbitration", value="Zeigt die aktuelle Arbitration an", inline=False)
    embed.add_field(name="?Rivenprice {arg1}", value="Zeigt dir den Preis eines Rivens z.B ?Rivenprice War", inline=False)
    embed.add_field(name="?Weapon {arg1}", value="Zeigt dir verschiedenste Weaponstats z.B ?Weapon Rubico", inline=False)
    embed.add_field(name="?Outpost", value="Zeigt dir ob eine Sentient Anomaly vorhanden ist ", inline=False)
    embed.add_field(name="?Darvo", value="Zeigt den aktuellen Deal für deine Plattform an ", inline=False)
    embed.add_field(name="?fissures", value="Zeigt die aktuellen Voidfissures anZeigt dir ob eine Sentient Anomaly vorhanden ist ", inline=False)
    embed.add_field(name="?buy {arg1}", value="Zeigt dir für dein Item die 5 meistbietenden Käufer auf Warframe Market an", inline=False)
    embed.add_field(name="?seller {arg1}", value="Zeigt dir für dein Item die 5 billigsten Verkäufer auf Warframe", inline=False)
    embed.add_field(name="?Arcane {arg1}", value="Zeigt verschiedneste Daten zu den Arkanas an ", inline=False)
    return(embed)

def HilfePS4():
    embed=discord.Embed(title="Help", description="my help page", color=0x0080ff)
    embed.set_thumbnail(url="")
    embed.add_field(name="&Arbitration", value="Zeigt die aktuelle Arbitration an", inline=False)
    embed.add_field(name="&Rivenprice {arg1}", value="Zeigt dir den Preis eines Rivens z.B &Rivenprice War", inline=False)
    embed.add_field(name="&Weapon {arg1}", value="Zeigt dir verschiedenste Weaponstats z.B &Weapon Rubico", inline=False)
    embed.add_field(name="&Outpost", value="Zeigt dir ob eine Sentient Anomaly vorhanden ist ", inline=False)
    embed.add_field(name="&Darvo", value="Zeigt den aktuellen Deal für deine Plattform an ", inline=False)
    embed.add_field(name="&fissures", value="Zeigt die aktuellen Voidfissures anZeigt dir ob eine Sentient Anomaly vorhanden ist ", inline=False)
    embed.add_field(name="&buy {arg1}", value="Zeigt dir für dein Item die 5 meistbietenden Käufer auf Warframe Market an", inline=False)
    embed.add_field(name="&seller {arg1}", value="Zeigt dir für dein Item die 5 billigsten Verkäufer auf Warframe", inline=False)
    embed.add_field(name="&Arcane {arg1}", value="Zeigt verschiedneste Daten zu den Arkanas an ", inline=False)
    return(embed)




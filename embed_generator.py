
import discord
from discord.ext import commands, tasks
import requests
import json

import FunktionenBot


def generate_embed_Arbitration(console):
    try:
        if console == "PC":
            Arbitration,node,enemy,type,Out3,Out4 = FunktionenBot.getArbitartionDataPC()
        if console == "SWI":
            Arbitration,node,enemy,type,Out3,Out4 = FunktionenBot.getArbitartionDataSWI()
        if console == "XB1":
            Arbitration,node,enemy,type,Out3,Out4 = FunktionenBot.getArbitartionDataPS4()
        if console == "PS4":
            Arbitration,node,enemy,type,Out3,Out4 = FunktionenBot.getArbitartionDataXB1()
        embed=discord.Embed(title="Arbitration", description="Hier ist die derzeit aktive Arbitration für deine Plattform", color=0x0080ff)
        embed.set_thumbnail(url="https://vignette.wikia.nocookie.net/warframe/images/b/b9/VitusEssence.png/revision/latest?cb=20190923095056")
        embed.add_field(name="Ort", value=str(Arbitration), inline=False)
        embed.add_field(name="Node", value=str(node), inline=False)
        embed.add_field(name="enemy", value=str(enemy), inline=False)
        embed.add_field(name="Missionstyp", value=str(type), inline=False)
        return(embed)
    except:
        print("Fehler in generate_embed (FunktionenBot.py)")

def generate_embed_Outpost(console):
    try:
        if console == "PC":
            node1,faction1,art1,active1,mission1 = FunktionenBot.get_Sentient_OutpostPC()
        if console == "SWI":
            node1,faction1,art1,active1,mission1 = FunktionenBot.get_Sentient_OutpostSWI()
        if console == "XB1":
            node1,faction1,art1,active1,mission1= FunktionenBot.get_Sentient_OutpostPS4()
        if console == "PS4":
            node1,faction1,art1,active1,mission1 = FunktionenBot.get_Sentient_OutpostXB1()
        embed=discord.Embed(title="Outpost "+str(console), description="Hier ist der Sentient-Outpost für deine Plattform", color=0x0080ff)
        embed.set_thumbnail(url="https://vignette.wikia.nocookie.net/warframe/images/3/31/SentientTrooper.png/revision/latest?cb=20151225162720&path-prefix=de")
        embed.add_field(name="Mission", value=str(node1), inline=False)
        embed.add_field(name="is active", value=str(active1), inline=False)
        embed.add_field(name="Faction", value=str(faction1), inline=False)
        embed.add_field(name="Missionstyp", value=str(art1), inline=False)
        return(embed)
    except:
        print("Fehler in generate_embed (FunktionenBot.py)")

def generate_embed_darvo(console):
    try:
        if console == "PC":
            item,originalprice,saleprice,total,sold,eta = FunktionenBot.Darvo_Deal_PC()
        if console == "SWI":
            item,originalprice,saleprice,total,sold,eta = FunktionenBot.Darvo_Deal_SWI()
        if console == "XB1":
            item,originalprice,saleprice,total,sold,eta = FunktionenBot.Darvo_Deal_XB1()
        if console == "PS4":
            item,originalprice,saleprice,total,sold,eta = FunktionenBot.Darvo_Deal_PS4()

        embed=discord.Embed(title="Darvo Deal "+str(console), description="Hier ist der aktuelle Deal", color=0x0080ff)
        embed.set_thumbnail(url="https://vignette.wikia.nocookie.net/warframe/images/7/7d/DarvoCodex.png/revision/latest/top-crop/width/360/height/450?cb=20150518154630")
        embed.add_field(name="Item", value=str(item), inline=False)
        embed.add_field(name="OriginalPrice", value=str(originalprice), inline=False)
        embed.add_field(name="SalePrice", value=str(saleprice), inline=False)
        embed.add_field(name="Anzahl komplett", value=str(total), inline=False)
        embed.add_field(name="Anzahl verkauft", value=str(sold), inline=False)
        embed.add_field(name="Time left", value=str(eta), inline=False)
        return(embed)
    except:
        print("Fehler in generate_embed_darvo(FunktionenBot.py)")